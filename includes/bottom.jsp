		<footer id="site-footer" role="contentinfo">
	        <div class="container">
	            <div class="region-footer">
	                <nav id="footer-menu" aria-label="resources">
	                    <div class="region region-footer-menu">
	                        <section id="block-menu-menu-footer-menu" class="block block-menu clearfix">
	                            <ul class="menu nav">
	                                <li class="first expanded dropdown marked-as-expanded" tabindex="0">
	                                    <span data-target="#" class="dropdown-toggle nolink" data-toggle="dropdown">Login <span class="caret"></span></span>
	                                    <ul class="dropdown-menu">
	                                        <li class="first leaf duplicate-link login marked-as-expanded"><a href="//my.csusb.edu/">myCoyote<span class="sr-only"> Login </span></a></li>
	                                        <li class="leaf duplicate-link login"><a href="http://csusb.blackboard.com/">Blackboard<span class="sr-only"> Login </span></a></li>
	                                        <li class="leaf duplicate-link login"><a href="http://mail.coyote.csusb.edu/">Student Email<span class="sr-only"> Login </span></a></li>
	                                        <li class="leaf duplicate-link login"><a href="http://outlook.com/csusb.edu">Faculty &amp; Staff Email<span class="sr-only"> Login </span></a></li>
	                                        <li class="last leaf"><a href="//www.csusb.edu/cas?destination=/student-affairs">Drupal Login</a></li>
	                                    </ul>
	                                </li>
	                                <li class="expanded dropdown marked-as-expanded" tabindex="0">
	                                    <span data-target="#" class="dropdown-toggle nolink" data-toggle="dropdown">Of Interest to... <span class="caret"></span></span>
	                                    <ul class="dropdown-menu">
	                                        <li class="first leaf duplicate-link of-interest-to"><a href="//www.csusb.edu/future-students"><span class="sr-only">Of Interest to  </span>Future Students</a></li>
	                                        <li class="leaf duplicate-link of-interest-to"><a href="//www.csusb.edu/current-students"><span class="sr-only">Of Interest to  </span>Current Students</a></li>
	                                        <li class="leaf duplicate-link of-interest-to"><a href="//www.csusb.edu/faculty-staff"><span class="sr-only">Of Interest to  </span>Faculty &amp; Staff</a></li>
	                                        <li class="leaf duplicate-link of-interest-to"><a href="//www.csusb.edu/community-visitors"><span class="sr-only">Of Interest to  </span>Community &amp; Visitors</a></li>
	                                        <li class="leaf duplicate-link of-interest-to"><a href="//www.csusb.edu/alumni-friends"><span class="sr-only">Of Interest to  </span>Alumni &amp; Friends</a></li>
	                                        <li class="leaf duplicate-link of-interest-to"><a href="//advancement.csusb.edu/development/corporate-foundation-relations"><span class="sr-only">Of Interest to  </span>University Partners</a></li>
	                                        <li class="last leaf duplicate-link of-interest-to"><a href="http://veterans.csusb.edu/"><span class="sr-only">Of Interest to  </span>Military / Veterans</a></li>
	                                    </ul>
	                                </li>
	                                <li class="expanded dropdown marked-as-expanded" tabindex="0">
	                                    <span data-target="#" class="dropdown-toggle nolink" data-toggle="dropdown">Employment <span class="caret"></span></span>
	                                    <ul class="dropdown-menu">
	                                        <li class="first leaf duplicate-link employment"><a href="http://agency.governmentjobs.com/csusb/">Job Listings<span class="sr-only"> Employment </span></a></li>
	                                        <li class="leaf duplicate-link employment"><a href="http://agency.governmentjobs.com/csusb/default.cfm?transfer=1">Faculty Jobs<span class="sr-only"> Employment </span></a></li>
	                                        <li class="leaf duplicate-link employment"><a href="http://career.csusb.edu/">Career Center<span class="sr-only"> Employment </span></a></li>
	                                        <li class="leaf duplicate-link employment"><a href="http://hr.csusb.edu/">Human Resources<span class="sr-only"> Employment </span></a></li>
	                                        <li class="last leaf"><a href="http://hr.csusb.edu/studentEmployment.html">Student Employment </a></li>
	                                    </ul>
	                                </li>
	                                <li class="last expanded dropdown marked-as-expanded" tabindex="0">
	                                    <span data-target="#" class="dropdown-toggle nolink" data-toggle="dropdown">Campus Services <span class="caret"></span></span>
	                                    <ul class="dropdown-menu">
	                                        <li class="first leaf duplicate-link campus-services"><a href="http://police.csusb.edu/">Public Safety<span class="sr-only"> Campus Services </span></a></li>
	                                        <li class="leaf duplicate-link campus-services"><a href="http://healthcenter.csusb.edu/">Student Health Center<span class="sr-only"> Campus Services </span></a></li>
	                                        <li class="leaf duplicate-link campus-services"><a href="http://psychcounseling.csusb.edu/">Psychological Counseling<span class="sr-only"> Campus Services </span></a></li>
	                                        <li class="leaf duplicate-link campus-services"><a href="//www.csusb.edu/housing">Housing &amp; Residential Life<span class="sr-only"> Campus Services </span></a></li>
	                                        <li class="leaf duplicate-link campus-services"><a href="http://ssd.csusb.edu">Services to Students with Disabilities<span class="sr-only"> Campus Services </span></a></li>
	                                        <li class="leaf duplicate-link campus-services"><a href="http://library.csusb.edu">Pfau Library<span class="sr-only"> Campus Services </span></a></li>
	                                        <li class="leaf duplicate-link campus-services"><a href="//www.csusb.edu/advising">Advising<span class="sr-only"> Campus Services </span></a></li>
	                                        <li class="leaf duplicate-link campus-services"><a href="http://parking.csusb.edu/">Parking &amp; Transportation<span class="sr-only"> Campus Services </span></a></li>
	                                        <li class="last leaf duplicate-link campus-services"><a href="//www.csusb.edu/registrar/records/request-csusb-transcript">Transcripts<span class="sr-only"> Campus Services </span></a></li>
	                                    </ul>
	                                </li>
	                            </ul>
	                        </section>
	                    </div>
	                </nav>
	            </div>
	            <div class="csusb-separator"></div>
	            <address class="site-address">
	          <span>California State University, San Bernardino </span>
	          <span class="location"><i class="fa fa-map-marker"></i>5500 University Parkway, San Bernardino CA 92407 </span>
	          <span class="phone"><i class="fa fa-phone"></i>+1 (909) 537-5000 </span>
	        </address>
	            <div class="region-social-footer">
	                <nav id="sc-menu" aria-label="social media">
	                    <div class="region region-footer-social">
	                        <section id="block-menu-menu-footer-social-links" class="block block-menu clearfix">
	                            <ul class="menu nav">
	                                <li class="first leaf fb">
	                                    <a href="http://www.facebook.com/CSUSB"><img src="//www.csusb.edu/sites/all/themes/csusb/images/footer/facebook-icon.svg" alt="Facebook"></a>
	                                </li>
	                                <li class="leaf tw">
	                                    <a href="http://www.twitter.com/csusbnews"><img src="//www.csusb.edu/sites/all/themes/csusb/images/footer/twitter-icon.svg" alt="Twitter"></a>
	                                </li>
	                                <li class="leaf yt">
	                                    <a href="//www.youtube.com/csusb"><img src="//www.csusb.edu/sites/all/themes/csusb/images/footer/youtube-icon.svg" alt="Youtube"></a>
	                                </li>
	                                <li class="leaf ig">
	                                    <a href="//instagram.com/csusb"><img src="//www.csusb.edu/sites/all/themes/csusb/images/footer/instagram-icon.svg" alt="Instagram"></a>
	                                </li>
	                                <li class="leaf li">
	                                    <a href="//www.linkedin.com/edu/school?id=17833"><img src="//www.csusb.edu/sites/all/themes/csusb/images/footer/linkedin-icon.svg" alt="LinkedIn"></a>
	                                </li>
	                                <li class="leaf cc">
	                                    <a href="http://blogs.csusb.edu/coyotecalling"><img src="//www.csusb.edu/sites/all/themes/csusb/images/footer/coyote-icon.svg" alt="Coyote Calling Blog"></a>
	                                </li>
	                                <li class="leaf ma">
	                                    <a href="//mobileapps.csusb.edu/"><img src="//www.csusb.edu/sites/all/themes/csusb/images/footer/apps-icon.svg" alt="Mobile Apps"></a>
	                                </li>
	                                <li class="last leaf sm">
	                                    <a href="http://socialmedia.csusb.edu/"><img src="//www.csusb.edu/sites/all/themes/csusb/images/footer/social-icon.svg" alt="Social Media Directory"></a>
	                                </li>
	                            </ul>
	                        </section>
	                    </div>
	                </nav>
	            </div>
	        </div>
	    </footer>
	    <div class="container">
	        <div class="region-after-footer">
	            <nav id="after-footer-menu" aria-label="policies and plugins footer">
	                <div class="region region-after-footer">
	                    <section id="block-menu-menu-footer-utility-menu-left" class="block block-menu clearfix">
	                        <ul class="menu nav">
	                            <li class="first leaf"><a href="//www.csusb.edu/about-csusb/accessibility">Accessibility</a></li>
	                            <li class="leaf"><a href="http://titleix.csusb.edu/#titleixNotice">Title IX Notice</a></li>
	                            <li class="leaf"><a href="//www.csusb.edu/about-csusb/contact-csusb">Contact</a></li>
	                            <li class="leaf"><a href="http://policies.csusb.edu/">Policies</a></li>
	                            <li class="last leaf"><a href="//www.csusb.edu/about-csusb/privacy-and-security-notice">Privacy and Security</a></li>
	                        </ul>
	                    </section>
	                    <section id="block-menu-menu-footer-utility-menu-right" class="block block-menu clearfix">
	                        <ul class="menu nav">
	                            <li class="first leaf"><a href="http://admissions.csusb.edu/contact/disclosure.shtml">Disclosure of Consumer Information</a></li>
	                            <li class="leaf"><a href="http://www.adobe.com/products/acrobat/readstep2.html">Acrobat Reader</a></li>
	                            <li class="last leaf"><a href="//www.microsoft.com/en-us/download/office.aspx">MS Office Viewers</a></li>
	                        </ul>
	                    </section>
	                </div>
	            </nav>
	        </div>
	    </div>


    </body>
</html>
