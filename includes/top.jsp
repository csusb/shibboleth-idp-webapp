<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <link rel="icon" href="favicon.ico">
    <link rel="shortcut icon" href="favicon.ico">
    <!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> -->
    <link type="text/css" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" media="all">
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,600italic,700,700italic,300,300italic&amp;obnpl9" media="all">
    <link rel="stylesheet" href="/idp/css/style.css">
    <link rel="stylesheet" href="/idp/css/site.css">

    <script src="//code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.8.24/jquery-ui.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="/idp/js/header.js"></script>
    <script src="/idp/js/site.js"></script>

    <title><spring:message code="idp.title" text="Shibboleth IdP" /></title>
</head>

<body class="html not-front not-logged-in one-sidebar sidebar-first page-node node-type-page anniversary">
    <!--<div id="skip-link">
        <a href="#page-content" tab-index="-1" class="element-invisible element-focusable">Skip banner navigation</a>
        <a href="#main-content" tab-index="0" class="element-invisible element-focusable">Skip to main content</a>
    </div>-->
    <nav class="top-nav" role="navigation" aria-label="university top">
        <div id="site-topbar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10">
                        <a href="#topbar-ql" class="ql-toggle">Quicklinks</a>
                        <div id="topbar-ql">
                            <div class="region region-topbar-left">
                                <section class="block block-menu contextual-links-region clearfix">

                                    <ul class="menu nav">
                                        <li class="first last leaf"><a href="https://my.csusb.edu/">myCoyote</a>
                                        </li>
                                    </ul>
                                </section>
                                <section id="block-menu-menu-header-navigation-right" class="block block-menu contextual-links-region clearfix">

                                    <ul class="menu nav">
                                        <li class="first leaf"><a href="http://phonebook.csusb.edu/">Directory</a>
                                        </li>
                                        <li class="leaf"><a href="http://www.csusb.edu/maps-directions">Map &amp; Directions</a>
                                        </li>
                                        <li class="last leaf"><a href="https://advancement.csusb.edu/development/how-give">Support CSUSB</a>
                                        </li>
                                    </ul>
                                </section>
                            </div>
                        </div>
                    </div>

                    <div id="topbar-right-wrapper" class="col-sm-2">
                        <div class="region region-topbar-right">
                            <section id="block-csusb-base-ft-fn-hdft-form" class="block block-csusb-base-ft-fn-hdft contextual-links-region clearfix">

                                <form class="search-form" action="/" method="post" id="csusb-base-ft-fn-hdft-search-form" accept-charset="UTF-8">
                                    <div>
                                        <div class="form-inline form-wrapper form-group" role="search" id="edit-basic">
                                            <div class="form-item form-item-keys form-type-textfield form-group">
                                                <label class="control-label" for="edit-keys">Enter your keywords </label>
                                                <input class="form-control form-text" type="text" id="edit-keys" name="keys" value="" size="40" maxlength="255">
                                            </div>
                                            <button type="submit" id="edit-submit" name="op" value="Search" class="btn btn-primary form-submit">Search</button>
                                        </div>
                                        <input type="hidden" name="form_build_id" value="form-seJOov35K5gEWwsvPypAIPXZmzaonsJLyjIwu39L8mY">
                                        <input type="hidden" name="form_token" value="tS1nhOGnZWegUjhWATRSOg2Rk6k8iUsomQs9BMBzTOI">
                                        <input type="hidden" name="form_id" value="csusb_base_ft_fn_hdft_search_form">
                                    </div>
                                </form>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar-header">
            <div class="container">
                <button type="button" class="menu-btn collapsed" data-toggle="collapse" data-target="#main-nav" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <div class="menu-icon"><span></span></div>
                </button>
                <a class="csusb-logo" href="//www.csusb.edu/" tabindex="0">
                    <img class="logo" src="//www.csusb.edu/sites/all/themes/csusb/images/header/logo.png" alt="CSUSB">
                </a>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="main-nav">
            <div class="container">
                <ul class="menu nav navbar-nav">
                    <li class="leaf"><a href="https://www.csusb.edu/admissions/">Admissions</a></li>

                    <li class="leaf"><a href="https://www.csusb.edu/colleges-and-academic-departments">Academics</a></li>

                    <li class="leaf"><a href="http://library.csusb.edu/">Library</a></li>

                    <li class="leaf"><a href="http://www.csusbathletics.com/index.aspx">Athletics</a></li>
                    
                    <li class="leaf"><a href="https://www.csusb.edu/campus-life">Campus Life</a></li>
                    
                    <li class="leaf"><a href="https://www.csusb.edu/about-csusb">About Us</a></li>

                </ul>
            </div>
        </div>
    </nav>
