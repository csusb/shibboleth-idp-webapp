/* site.js */

Date.prototype.getDayName = function() {
    var names = ["Sunday", "Monday", "Tuesday",
                 "Wednesday", "Thursday", "Friday",
                 "Saturday"];
    return names[this.getDay()];
};

Date.prototype.getMonthName = function() {
    var names = ["January", "February", "March",
                 "April", "May", "June", "July",
                 "August", "September", "October",
                 "November", "December"];
    return names[this.getMonth()];
};

Date.prototype.toString = function() {
    return this.getDayName() + ", " + this.getMonthName() + " " + this.getDate() + ", " + this.getFullYear();
};

$(document).bind("ready", function() {

    $('form#csusb-base-ft-fn-hdft-search-form').submit(function(e){
        e.preventDefault();
        window.location.href= 'https://search.csusb.edu/gsearch/' + encodeURIComponent($('#edit-keys').val());
    });

    var date = new Date();
    switch(date.getMonth()) {
        case 0:
            $('body').css('background-image', 'url("/idp/images/bg_cycle/1.jpg")');
            break;
        case 1:
            $('body').css('background-image', 'url("/idp/images/bg_cycle/1.jpg")');
            break;
        case 2:
            $('body').css('background-image', 'url("/idp/images/bg_cycle/2.jpg")');            
            break;
        case 3:
            $('body').css('background-image', 'url("/idp/images/bg_cycle/2.jpg")');
            break;
        case 4:
            $('body').css('background-image', 'url("/idp/images/bg_cycle/3.jpg")');
            break;
        case 5:
            $('body').css('background-image', 'url("/idp/images/bg_cycle/3.jpg")');
            break;
        case 6:
            $('body').css('background-image', 'url("/idp/images/bg_cycle/4.jpg")');
            break;
        case 7:
            $('body').css('background-image', 'url("/idp/images/bg_cycle/4.jpg")');
            break;
        case 8:
            $('body').css('background-image', 'url("/idp/images/bg_cycle/5.jpg")');
            break;
        case 9:
            $('body').css('background-image', 'url("/idp/images/bg_cycle/5.jpg")');
            break;
        case 10:
            $('body').css('background-image', 'url("/idp/images/bg_cycle/6.jpg")');
            break;
        case 11:
            $('body').css('background-image', 'url("/idp/images/bg_cycle/6.jpg")');
            break;
        default:
            $('body').css('background-image', 'url("/idp/images/bg_cycle/1.jpg")');
    }
    
});
