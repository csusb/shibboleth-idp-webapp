$(document).ready(function(){
    /* @desc Handle Top Bar Left Quicklinks toggling
    **/
    $('#site-topbar .ql-toggle').click(function(e) {
        e.preventDefault();
        $('#topbar-ql').slideToggle();
        $(this).toggleClass('open');
    });

    /* @desc Shift focus after following skip navigation links.
    **/
    $('#skip-link a').click(function(e){
        $($(this).attr('href')).focus();
    });

    /* @desc Hide H1s on Home Pages, identified by large banner
     **/
    if($('.region-header .banner-content') && $('.region-header .banner-content').hasClass('big')) {
        $('H1').addClass('sr-only');
    }

    /* @desc Add spacing to pages without banners to prevent H1s overlapping
     **/
    if (!$('.region-header .banner-content').length) {
        $('.main-container .container [role="main"]').css('margin-top', '68px');
    }

    /*
     * @desc Toggle mechanism for menu blocks
     **/
    $('.block-menu-block .nav li:has(ul) > a').each( function(e) {
        var item = $(this);

        item.append('<span class="toggle" tabindex="0"></span>');

        item.children('.toggle').click( function(event) {
          item.parent().toggleClass('open');
          return false;
        }).keypress(function(e) {
          if (e.which == 13) {    // trigger click event on enter keydown
            $(this).trigger('click');
          }
        });

        if (item.parent().hasClass('active-trail')) {
          item.parent().children('a').children('.toggle').click();
        }
        });

        $('#main-menu .nav li:has(ul) > a').each( function(e) {
        var item = $(this);

        item.append('<span class="toggle"></span>');
    });


    /*
    * @desc  Navigation accessability features for desktop view only.
    **/

    function menuAccessibility(){
        // Disable current Bootstrap click() functionality.
        $('.navbar-nav .dropdown .dropdown-toggle').click(function() {
            return false;
        });

        // Expand dropdown menu on hover.
        $('.navbar-nav .dropdown').on('mouseover',function() {
            $('.navbar-nav .dropdown.open').removeClass('open');
            $(this).addClass('open');
            $('.top-nav .overlay').addClass('show');
        }).mouseleave(function() {
            $(this).removeClass('open');
            $('.top-nav .overlay').removeClass('show');
            $(this).blur();
        });

        // Expand menu on keyboard focus.
        $('.navbar-nav .dropdown').focus(function() {
            $('.navbar-nav .dropdown.open').removeClass('open');
            $('.top-nav .overlay').addClass('show');
            $(this).addClass('open');
        });

        // If last menu item has a dropdown menu, collapse on focus out.
        $('.navbar-nav > li:last-child > .dropdown-menu > li > .menu > li:last-child > .menu > li:last-child a').focusout(function() {
            $('.navbar-nav .dropdown.open').removeClass('open');
            $('.top-nav .overlay').removeClass('show');
        });

        // Close dropdown if non-dropdown menu items focus.
        $('.navbar-nav > li > a').focus(function() {
            $('.navbar-nav .dropdown.open').removeClass('open');
            $('.top-nav .overlay').removeClass('show');
        });

        // Make logo tabable.
        $('a.csusb-logo').attr('tabindex', '0');

        // Close dropdown if user 'shift-tab's away from menu.
        $('a.csusb-logo').focus(function() {
            $('.navbar-nav .dropdown').removeClass('open');
            $('.top-nav .overlay').removeClass('show');
        });
    }

    checkSize()
    $(window).resize(checkSize);


    function checkSize(){
        if($('.top-nav .navbar-header .menu-btn').css("display") == "none") {
            menuAccessibility();
        } else {
            return;
        }
        if ($(window).width() > 768) {
           menuAccessibility();
        }
    }
});