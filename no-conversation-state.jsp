<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>

<%@include file="includes/top.jsp" %>

<div id="content" class="container">
	<div class="alert alert-danger">
		<h2>Authentication Error</h2>
		<p><spring:message code="shibCasAuthn3.errorMessage" text="There was
		a problem finding your session. This can happen if you waited too long on the
		login page, or if you were redirected to a different server that issued the
		original request. This error usually goes away if you try accessing your
		desired application again." />
		</p>
	</div>
</div>

<%@include file="includes/bottom.jsp" %>
