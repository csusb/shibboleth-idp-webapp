import os
from fabric.api import *
from fabtools.deb import update_index
from fabtools.deb import install

@task
def metadata():
    kvpairs = [line.partition(':') for line in local('dpkg-parsechangelog',
                                                     capture=True).split('\n')]
    env.deb_info = {pair[0]: pair[2].strip() for pair in kvpairs}
    env.deb_filename = '{Source}_{Version}_all.deb'.format(**env.deb_info)
    env.deb_changes_filename  = "{Source}_{Version}_amd64.changes".format(**env.deb_info)

@task(default=True)
def build():
    execute(metadata)
    """Build deb. Resulting .deb placed under deb/ directory."""
    local("dpkg-buildpackage -b -uc")
    try:
        os.mkdir('deb')
    except OSError:
        pass
    os.rename(
        os.path.join('..', env.deb_filename),
        os.path.join('deb', env.deb_filename),
    )
    os.rename(
        os.path.join('..', env.deb_changes_filename),
        os.path.join('deb', env.deb_changes_filename),
    )

@task
@hosts('apt.iset.blue')
def upload():
    """Upload deb to APT repository and update repository indexes"""
    execute(metadata)
    src = 'deb/{deb_filename}'.format(**env)
    tmpdir = run('mktemp -d')
    try:
        put(src, tmpdir)
        run('chmod g+rx {}'.format(tmpdir))
        run('chgrp apt {}'.format(tmpdir))
        with cd(tmpdir):
            sudo('aptly task run repo add iset {deb_filename}, publish update jessie iset'.format(**env), user='apt')
    finally:
        run('rm -rf {}'.format(tmpdir))

@task
@roles('idp')
def update():
    update_index()
    install('shibboleth-idp3-webapp-branded')
