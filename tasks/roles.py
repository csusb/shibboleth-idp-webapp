from fabric.api import env
from boto import connect_ec2


class EC2Role(object):
    def __init__(self, role_name):
        self.name = role_name

    def __iter__(self):
        ec2 = connect_ec2()
        reservations = ec2.get_all_instances(filters={'tag:Role': self.name})
        for r in reservations:
            for i in r.instances:
                yield i.public_dns_name


env.roledefs.update({
    'idp': EC2Role('shibboleth-idp-3'),
})
