<%@ page isErrorPage="true" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>

<%@ include file="includes/top.jsp" %>

<div id="content" class="container">
	<div class="alert alert-danger">
		<h2>An unexepected error occured</h2>
		<%= exception.getMessage() %>
	</div>
</div>

<%@ include file="includes/bottom.jsp" %>
