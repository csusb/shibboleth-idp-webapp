<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>

<%@ include file="includes/top.jsp" %>

<div id="content" class="container">
	<div class="alert alert-danger">
	  <spring:message code="root.message" text="No services are available at this location." />
	</div>
</div>

<%@ include file="includes/bottom.jsp" %>
